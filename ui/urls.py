from django.conf.urls import url, include
from .views import TestList, TestDetail, AddQuestion
urlpatterns=[
    url(r'^test/(?P<pk>\d+)/question/', AddQuestion.as_view()),
    url(r'^test/(?P<pk>\d+)/', TestDetail.as_view()),
    url(r'^', TestList.as_view()),
]
