from django.contrib import admin
from .models import Question,Option,Test
# Register your models here.
admin.site.register(Test)
admin.site.register(Option)

class OptionInline(admin.StackedInline):
    model= Option

class QuestionAdmin(admin.ModelAdmin):
    inlines = [ OptionInline, ]

admin.site.register(Question,QuestionAdmin)
