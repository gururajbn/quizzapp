from rest_framework import serializers
from .models import Question, Option, Test

class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model= Test
        fields= ('id', 'title', 'created', 'question_limit', 'question_count')

class OptionSerializer(serializers.ModelSerializer):
    class Meta:
        model= Option
        fields = ('id','option','image')

class QuestionSerializer(serializers.ModelSerializer):
    options= OptionSerializer(many=True)
    
    class Meta:
        model= Question
        fields= ('id','question','image','created', 'options','test')

    def create(self, validated_data):
        options_data= validated_data.pop('options')
        question= Question.objects.create(**validated_data)
        for option in options_data:
            Option.objects.create(question=question,**option)
        return question
