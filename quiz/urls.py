from django.conf.urls import url, include
from rest_framework import routers
from .views import QuestionViewset, OptionViewset, TestViewSet

router = routers.DefaultRouter()
router.register(r'quiz', QuestionViewset)
router.register(r'option', OptionViewset)
router.register(r'test', TestViewSet)

urlpatterns = router.urls
